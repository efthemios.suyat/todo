//
//  TodoItem.swift
//  todo
//
//  Created by Efthemios Prime on 9/7/16.
//  Copyright © 2016 Efthemios Prime. All rights reserved.
//

import Foundation


struct TodoItem {
    let title: String
}