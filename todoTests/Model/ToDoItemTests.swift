//
//  ToDoItemTests.swift
//  todo
//
//  Created by Efthemios Prime on 9/7/16.
//  Copyright © 2016 Efthemios Prime. All rights reserved.
//
// https://www.andrewcbancroft.com/2014/07/22/swift-access-control-implications-for-unit-testing/
//

import XCTest
@testable import todo

class ToDoItemTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    // add a title property
    func testInit_ShouldTakeTitle() {
        let item = TodoItem(title: "Test title")
        XCTAssertNotNil(item, "item should not be nil")
    }

}
